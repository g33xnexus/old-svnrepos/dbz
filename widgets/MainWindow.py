from PyQt4 import QtCore, QtGui, uic
import sys, os

class MainWindow (QtGui.QApplication):
	editors = {}

	def __init__ (self, *args):
		''' Initialize the main window.
		'''
		apply (QtGui.QApplication.__init__, (self, sys.argv) + args)
		self.openEditors = list ()

	def edit (self, data = None, noDefaultEditor = False):
		''' Open an editor to edit the given data.
		'''
		if data is None:
			if noDefaultEditor:
				return False
			newEditor = MainWindow.editors["default"] (self, data)
		else:
			if data.__class__.__name__ in MainWindow.editors:
				newEditor = MainWindow.editors[data.__class__.__name__] (self, data)
			elif noDefaultEditor != True:
				newEditor = MainWindow.editors["default"] (self, data)
			else:
				return False
		self.openEditors.append (newEditor)
		newEditor.show ()

	def run (self):
		''' Start the main runloop.
		'''
		print "Starting runloop."
		self.exec_ ()

	def quit (self, editor):
		''' Exit the given editor, or the entire application if this is the last editor.
		'''
		editor.close ()


# Find all editor modules, load them, and catalog what data types they support.
for file in os.listdir ("editors"):
	if file[-3:] == ".py" and file != "__init__.py":
		module = __import__ ('editors', globals (), locals (), [file[:-3], ])
		realModule = module.__dict__[file[:-3]]
		for tp in realModule.editor.types:
			MainWindow.editors[tp] = realModule.editor

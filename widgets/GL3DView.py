from PyQt4 import QtCore, QtGui, QtOpenGL
from OpenGL import GL, GLU
from libdbz import geom, DataObject, extremes
import math

class ColorScheme:
	def __init__ (self):
		self.background = QtGui.QColor (0, 0, 0)
		self.axes = QtGui.QColor (0, 255, 127, 191)
		self.grid = QtGui.QColor (0, 63, 127)
		self.vertices = QtGui.QColor (0, 255, 0)
		self.lines = QtGui.QColor (0, 255, 127)


class GridRender:
	def __init__ (self, widget):
		self.widget = widget
		self.gridSize = 8
		self.gridHighlight = 4

	def preProcess (self, obj):
		pass

	def render (self):
		# TODO: Limit number of grid dots; if they're closer than n pixels, go to the next grid size.
		extents = self.widget.getExtents ()

		gridSizeExponent = math.floor (-0.71 * self.widget.getScaleExponent () + 1)
		if gridSizeExponent < 0:
			gridSizeExponent = 0
		gridStep = self.gridSize * (self.gridHighlight ** gridSizeExponent)

		xMin = int (math.floor (extents.xMin / gridStep) * gridStep)
		yMin = int (math.floor (extents.yMin / gridStep) * gridStep)
		zMin = int (math.floor (extents.zMin / gridStep) * gridStep)

		xMax = int (math.floor (extents.xMax / gridStep) * gridStep)
		yMax = int (math.floor (extents.yMax / gridStep) * gridStep)
		zMax = int (math.floor (extents.zMax / gridStep) * gridStep)


		# Set the blend function to alpha.
		GL.glBlendFunc (GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

		self.widget.qglColor (self.widget.colorScheme.axes)
		GL.glBegin (GL.GL_LINES)

		# X Axis
		GL.glVertex3d (xMin, 0, 0)
		GL.glVertex3d (xMax, 0, 0)

		# Y Axis
		GL.glVertex3d (0, yMin, 0)
		GL.glVertex3d (0, yMax, 0)

		GL.glEnd ()


		# Set the blend function to additive.
		GL.glBlendFunc (GL.GL_ONE, GL.GL_ONE)

		self.widget.qglColor (self.widget.colorScheme.grid)
		GL.glBegin (GL.GL_POINTS)
		for x in range (xMin, xMax + 1, int (gridStep)):
			for y in range (yMin, yMax + 1, int (gridStep)):
				GL.glVertex3d (x, y, 0)

				# If this point should be highlighted, draw it again. (additive)
				power = 1
				while x % (gridStep * (self.gridHighlight ** power)) == 0 and power <= 5:
					GL.glVertex3d (x, y, 0)
					power += 1

				power = 1
				while y % (gridStep * (self.gridHighlight ** power)) == 0 and power <= 5:
					GL.glVertex3d (x, y, 0)
					power += 1
		GL.glEnd ()


class VertexRender:
	def __init__ (self, widget):
		self.widget = widget
		self.vertices = list ()

	def preProcess (self, obj):
		if isinstance (obj, geom.Vertex):
			self.vertices.append (obj)

	def render (self):
		# Set the blend function to alpha.
		GL.glBlendFunc (GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

		self.widget.qglColor (self.widget.colorScheme.vertices)
		GL.glBegin (GL.GL_POINTS)
		for vtx in self.vertices:
			GL.glVertex3d (vtx.x, vtx.y, vtx.z)
		GL.glEnd ()


class PolyRender:
	def __init__ (self, widget):
		self.widget = widget
		self.polys = list ()

	def preProcess (self, obj):
		if isinstance (obj, geom.Polygon):
			self.polys.append (obj)

	def render (self):
		self.widget.qglColor (self.widget.colorScheme.lines)
		for poly in self.polys:
			#GL.glBegin (GL.GL_LINE_LOOP)
			GL.glBegin (GL.GL_POLYGON)
			for vtx in poly.getChildren ():
				if vtx.getType () in ("Vertex", "VertexRef"):
					GL.glVertex3d (
						vtx.position.x,
						vtx.position.y,
						vtx.position.z
					)
			GL.glEnd ()


class GL3DView (QtOpenGL.QGLWidget):
	__pyqtSignals__ = ("selectionChanged(int)")
	(
		MODE_2D,
		MODE_3D
	) = range (0, 2)

	class Extents (object):
		def __init__ (self, xMin, xMax, yMin, yMax, zMin, zMax):
			(self.xMin, self.xMax, self.yMin, self.yMax, self.zMin, self.zMax) = (xMin, xMax, yMin, yMax, zMin, zMax)

	def __init__ (self, parent = None, dataObject = None):
		QtOpenGL.QGLWidget.__init__ (self, parent)

		self.colorScheme = ColorScheme ()

		self.rootItem = dataObject

		self.delegates = list ()
		self.delegates.append (GridRender (self))
		self.delegates.append (VertexRender (self))
		self.delegates.append (PolyRender (self))

		self.center = geom.Vector (0, 0, 0)
		self.scaleExponent = 2.0
		self.matricesPushed = False
		self.matricesChanged = True
		self.extentsChanged = True

		self.lastMousePosition = geom.Vector ()

		self.mode = self.MODE_2D

	def getScaleExponent (self):
		return self.scaleExponent

	def setScaleExponent (self, scale):
		self.scaleExponent = scale
		self.matricesChanged = True
		self.extentsChanged = True

	def getCenter (self):
		return (self.center.x, self.center.y, self.center.z)

	def setCenter (self, x, y, z):
		self.center.x = x
		self.center.y = y
		self.center.z = z
		self.matricesChanged = True
		self.extentsChanged = True

	def getViewBox (self):
		if self.extentsChanged:
			self.topLeft = self.getWorldCoordinates (0, 0)
			self.bottomLeft = self.getWorldCoordinates (0, self.height)
			self.topRight = self.getWorldCoordinates (self.width, 0)
			self.bottomRight = self.getWorldCoordinates (self.width, self.height)
			self.extentsChanged = False
		return (self.topLeft, self.bottomLeft, self.topRight, self.bottomRight)

	def getExtents (self):
		box = self.getViewBox ()
		(xMax, yMax, zMax) = (extremes.uMin, extremes.uMin, extremes.uMin)
		(xMin, yMin, zMin) = (extremes.uMax, extremes.uMax, extremes.uMax)

		for corner in box:
			if corner.x < xMin:
				xMin = corner.x
			if corner.x > xMax:
				xMax = corner.x
			if corner.y < yMin:
				yMin = corner.y
			if corner.y > yMax:
				yMax = corner.y
			if corner.z < zMin:
				zMin = corner.z
			if corner.z > zMax:
				zMax = corner.z

		return self.Extents (xMin, xMax, yMin, yMax, zMin, zMax)

	def getWorldCoordinates (self, windowX, windowY):
		if self.matricesChanged:
			self.modelMatrix = GL.glGetDoublev (GL.GL_MODELVIEW_MATRIX)
			self.projMatrix = GL.glGetDoublev (GL.GL_PROJECTION_MATRIX)
			self.matricesChanged = False
		return geom.Vector (*GLU.gluUnProject (windowX, windowY, 0, self.modelMatrix, self.projMatrix))

	def setDataObject (self, dataObject):
		self.rootItem = dataObject

	def minimumSizeHint (self):
		return QtCore.QSize (50, 50)

	def sizeHint (self):
		return QtCore.QSize (400, 400)

	def initializeGL (self):
		#GL.glEnable (GL.GL_SMOOTH)
		GL.glEnable (GL.GL_POINT_SMOOTH)
		GL.glEnable (GL.GL_LINE_SMOOTH)
		GL.glEnable (GL.GL_BLEND)
		GL.glPointSize (7.0)
		#GL.glLineWidth (1.0)
		GL.glPolygonMode (GL.GL_FRONT_AND_BACK, GL.GL_LINE)

		if GL.GL_VERSION <= 7938:
			constants = {
				"GL_LINE_WIDTH_RANGE": GL.GL_LINE_WIDTH_RANGE,
				"GL_LINE_WIDTH_GRANULARITY": GL.GL_LINE_WIDTH_GRANULARITY,
				"GL_POINT_SIZE_RANGE": GL.GL_POINT_SIZE_RANGE,
				"GL_POINT_SIZE_GRANULARITY": GL.GL_POINT_SIZE_GRANULARITY
			}
		else:
			constants = {
				"GL_ALIASED_LINE_WIDTH_RANGE": GL.GL_ALIASED_LINE_WIDTH_RANGE,
				"GL_SMOOTH_LINE_WIDTH_RANGE": GL.GL_SMOOTH_LINE_WIDTH_RANGE,
				"GL_SMOOTH_LINE_WIDTH_GRANULARITY": GL.GL_SMOOTH_LINE_WIDTH_GRANULARITY,
				"GL_SMOOTH_POINT_SIZE_RANGE": GL.GL_SMOOTH_POINT_SIZE_RANGE,
				"GL_SMOOTH_POINT_SIZE_GRANULARITY": GL.GL_SMOOTH_POINT_SIZE_GRANULARITY,
				"GL_ALIASED_POINT_SIZE_RANGE": GL.GL_ALIASED_POINT_SIZE_RANGE,
				"GL_ALIASED_POINT_SIZE_GRANULARITY": GL.GL_ALIASED_POINT_SIZE_GRANULARITY
			}

		for name, constant in constants.iteritems ():
			print name + ": " + str (GL.glGetDoublev (constant))

	def paintGL (self):
		self.__getGeometry (self.rootItem)

		self.qglClearColor (self.colorScheme.background)
		GL.glClear (GL.GL_COLOR_BUFFER_BIT)

		for delegate in self.delegates:
			delegate.render ()

	def resizeGL (self, width, height):
		self.width = width
		self.height = height

		GL.glViewport (0, 0, width, height)

		self.updateTransform ()

	def updateTransform (self):
		GL.glMatrixMode (GL.GL_PROJECTION)
		if self.matricesPushed:
			GL.glPopMatrix ()

		# Start with an identity matrix.
		GL.glLoadIdentity ()

		# Set up a transformation for the viewport.
		if self.mode == self.MODE_2D:
			GLU.gluOrtho2D (0.0, self.width, self.height, 0.0)
			#GLU.gluOrtho2D (0.0, self.width, 0.0, self.height)
		else:
			GLU.gluPerspective (90.0, self.width / self.height, 0.0, 999999999)

		# Translate so that (0, 0, 0) is centered.
		GL.glTranslated (self.width / 2, self.height / 2, 0.0)

		# User-specified scaling, translation, and rotation.
		GL.glPushMatrix ()
		GL.glScaled (math.exp (self.scaleExponent), math.exp (self.scaleExponent), math.exp (self.scaleExponent))
		GL.glTranslated (self.center.x, self.center.y, self.center.z)

		GL.glMatrixMode (GL.GL_MODELVIEW)

		self.matricesPushed = True
		self.matricesChanged = True
		self.extentsChanged = True

	def mousePressEvent (self, event):
		if event.button () == QtCore.Qt.LeftButton:
			#TODO: Select whatever's under the cursor.
			self.lastMousePosition = (event.x (), event.y ())
			self.lastCenter = self.center
			event.ignore ()
		else:
			event.ignore ()

	def mouseMoveEvent (self, event):
		print "Buttons: %d; x: %d; y: %d" % (event.buttons (), event.x (), event.y ())
		if event.buttons () & QtCore.Qt.LeftButton != 0:
			#TODO: Move whatever's under the cursor, or drag-select.
			# For now, just pan the viewport.
			position = (event.x (), event.y ())
			delta = (
				position[0] - self.lastMousePosition[0],
				position[1] - self.lastMousePosition[1]
			)
			deltaWorld = geom.Vector (delta[0] / math.exp (self.scaleExponent), delta[1] / math.exp (self.scaleExponent))
			self.center = self.lastCenter + deltaWorld
			self.updateTransform ()
			self.updateGL ()
			event.ignore ()
		else:
			event.ignore ()

	def wheelEvent (self, event):
		self.scaleExponent += (event.delta () / 1000.0)
		self.updateTransform ()
		self.updateGL ()

	def contextMenuEvent (self, event):
		# Create and show the context menu.
		#menu = QtGui.QMenu (self)
		#menu.addAction ()
		#menu.exec (event.globalPos ())
		pass

	def __getGeometry (self, obj):
		for delegate in self.delegates:
			delegate.preProcess (obj)

		for child in obj.getChildren ():
			self.__getGeometry (child)


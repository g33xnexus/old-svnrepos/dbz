import xml.dom.minidom
import DataObject

class DbzFile:
	"""
	A class which represents a saved DataObject hierarchy.
	"""
	def __init__ (self):
		self.indent = 0

	def write (self, object):
		pass

	def serializeNew (self, object, container = None):
		if container == None:
			doc = xml.dom.minidom.Document ()
			rootElement = doc.createElementNS ("https://development.g33xnexus.com/projects/dbz", "dbzFile")
			doc.appendChild (rootElement)
			self.serializeNew (object, rootElement)
			return doc.write ()
		else:
			classname = object.getType ()
			outstr = "	" * self.indent + "<%s" % classname
			for key, value in object.getAttributes ():
				# Use XML to write element.
				pass
			if len (children) > 0:
				for obj in children:
					self.serializeNew (object, objElement)

	def serialize (self, object):
		classname = object.getType ()
		outstr = "	" * self.indent + "<%s" % classname
		for key, value in object.getAttributes ():
			outstr += " %s=\"%s\"" % (str (key), str (value))
		children = object.getChildren ()
		if len (children) > 0:
			outstr += ">\n"
			self.indent += 1
			for obj in children:
				outstr += self.serialize (obj)
			self.indent -= 1
			outstr += "	" * self.indent + "</%s>\n" % classname
		else:
			outstr += " />\n"
		return outstr


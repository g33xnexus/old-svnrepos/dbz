from PyQt4 import Qt, QtCore, QtGui
import resources

class DataObjectModel (QtCore.QAbstractItemModel):
	# DataObjectModel (const QString &data, QObject *parent = 0);
	def __init__ (self, data = None, parent = None):
		apply (QtCore.QAbstractItemModel.__init__, (self, ))

		# Initially, set rootItem to None, then use setDataObject().
		self.rootItem = None
		self.setDataObject (data)

		# Set up temporary variables for getRow() and getDataObjectByRow().
		self.getRowTemp = 0
		self.getDataObjectByRowTemp = 0

		# Map of item ids to items, for fast lookups.
		self.ids = dict ()

	def setDataObject (self, data):
		''' Set the DataObject which this model exposes.
		'''
		if self.rootItem is not None:
			self.rootItem.removeListener (self)

		self.rootItem = data

		if self.rootItem is not None:
			self.rootItem.addListener (self)
			self.__emitChanged (0, 0, self.rootItem)

	# QVariant data (const QModelIndex &index, int role) const;
	def data (self, index, role):
		returnDiagnostics = (role in (QtCore.Qt.DisplayRole, QtCore.Qt.ToolTipRole))

		# If we get an invalid index, return a diagnostic message.
		if not index.isValid ():
			if returnDiagnostics:
				return QtCore.QVariant ("Invalid index! " + self.__printModelIndex (index))
			else:
				return QtCore.QVariant ()

		# Find the DataObject the given index refers to.
		item = self.getDataObject (index, self.rootItem)

		# If we couldn't find the corresponding DataObject, return a diagnostic message.
		if item is None:
			if returnDiagnostics:
				return QtCore.QVariant ("Valid index, but no object! " + self.__printModelIndex (index))
			else:
				return QtCore.QVariant ()

		if role == QtCore.Qt.DisplayRole:
			# Display the name of the DataObject, if there is one, and the class name. (for subclasses of DataObject)
			return QtCore.QVariant (item.getName ())

		elif role == QtCore.Qt.ToolTipRole:
			# Display information about this DataObject.
			return QtCore.QVariant ("XML: " + str (item) +
				"\nIndex: " + self.__printModelIndex (index) +
				"\nParent: " + self.__printModelIndex (self.parent (index)))

		elif role == QtCore.Qt.DecorationRole:
			# Display the icon corresponding to the DataObject's class.
			return QtCore.QVariant (QtGui.QIcon (":/images/" + item.getType () + ".png"))

		else:
			return QtCore.QVariant ()

	# QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	def headerData (self, section, orientation, role):
		# Return no header data.
		return QtCore.QVariant ()

	# QModelIndex index (int row, int column, const QModelIndex &parent = QModelIndex ()) const;
	def index (self, row, column, parentIdx):
		# We only have one column.
		if column != 0:
			return QtCore.QModelIndex ()

		parent = self.getDataObject (parentIdx, self.rootItem)

		if parent is None:
			# If the parent isn't valid, look for the given row in the root DataObject.
			item = self.getDataObjectByRow (row, self.rootItem)
			item = self.rootItem
		else:
			# Look for the given row under parent.
			item = self.getDataObjectByRow (row, parent)

		# Find the model index of the child item.
		idx = self.__getIndex (item, row, column)
		return idx

	# QModelIndex parent (const QModelIndex &index) const;
	def parent (self, index):
		# Find the DataObject for the given model index.
		item = self.getDataObject (index, self.rootItem)

		if item is None:
			# If we can't find the DataObject, we can't find its parent either.
			return QtCore.QModelIndex ()
		else:
			# Find the model index of the DataObject's parent.
			idx = self.__getIndex (item.getParent ())
			return idx

	# int rowCount (const QModelIndex &parent = QModelIndex ()) const;
	def rowCount (self, parentIdx):
		if parentIdx.isValid ():
			parent = self.getDataObject (parentIdx, self.rootItem)
			if parent is None:
				# We can't find the parent DataObject, so blindly return 1.
				return 1
			else:
				# Find the number of children of the parent DataObject.
				count = parent.getChildCount ()
				#count = parent.getChildCountRecursive ()
				return count
		else:
			# There is only ever one root node.
			return 1

	# int columnCount (const QModelIndex &parent = QModelIndex ()) const;
	def columnCount (self, parent):
		# We always have exactly 1 column.
		return 1


	def __emitChanged (self, row, column, data):
		''' Emit the dataChanged signal recursively for every item.
		'''
		self.emit (QtCore.SIGNAL ("dataChanged"), self.__getIndex (data, row, column), self.__getIndex (data, row, column))

		row = 0
		for item in data.getChildren ():
			self.__emitChanged (row, column, item)
			row += 1

	def __getIndex (self, item, row = None, column = 0):
		''' Get the model index corresponding to the given DataObject.
		'''
		if item is None:
			return QtCore.QModelIndex ()

		try:
			return item.__index
		except AttributeError:
			# Figure out what row the DataObject is on.
			if row is None:
				row = self.__getRow (item, self.rootItem)

			# Create a model index for the DataObject.
			item.__index = self.createIndex (row, column, id (item))

			# Record this DataObject's id in self.ids.
			self.ids[id (item)] = item
			return item.__index

	def __getRow (self, needle, parent):
		''' Get the row of the given DataObject.
		needle is the item whose row number we're searching for.
		parent is the item that was last checked.
		'''
		#if needle == parent:
		#	return 0
		#else:
		#	row = 0
		#	for item in parent.getChildren ():
		#		if needle == item:
		#			return row
		#		row += 1
		#for item in parent.getChildren ():
		#	row = self.__getRow (needle, item)
		#	if row is not None:
		#		return row
		#return None
		if parent == needle:
			row = self.getRowTemp
			self.getRowTemp = 0
			return row
		else:
			for item in parent.getChildren ():
				self.getRowTemp += 1
				row = self.__getRow (needle, item)

				if row is not None:
					return row
		self.getRowTemp = 0
		return None

	def getDataObjectByRow (self, row, parent = None):
		''' Get the DataObject at the given row under the given parent.
		row is the row number we're searching for.
		parent is the item that was last checked.
		'''
		if parent is None:
			parent = self.rootItem
		return parent.getChild (row)
		#if self.getDataObjectByRowTemp == row:
		#	self.getDataObjectByRowTemp = 0
		#	return parent
		#else:
		#	for item in parent.getChildren ():
		#		self.getDataObjectByRowTemp += 1
		#		found = self.getDataObjectByRow (row, item)

		#		if found is not None:
		#			return found
		#self.getDataObjectByRowTemp = 0
		#return None

	def getDataObject (self, index, parent = None):
		''' Get the DataObject with the given model index.
		index is the index we're searching for.
		parent is the item that was last checked.
		'''
		# TODO: Use internalId () to get the object?
		if index.internalId () in self.ids:
			return self.ids[index.internalId ()]
		else:
			if parent is None:
				parent = self.rootItem
			if self.__getIndex (parent) == index:
				return parent
			else:
				for item in parent.getChildren ():
					return self.getDataObject (index, item)
			return None

	def __printModelIndex (self, idx):
		''' Pretty-print a model index.
		'''
		if idx.isValid ():
			return str (idx) + " -> " + str (self.getDataObject (idx, self.rootItem))
		else:
			return "invalid " + str (idx)


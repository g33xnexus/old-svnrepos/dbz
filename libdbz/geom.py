from DataObject import DataObject

class Vector (object):
	## Overrides ##
	def __init__ (self, x = 0, y = 0, z = 0):
		self.x = x
		self.y = y
		self.z = z

	def __str__ (self):
		''' Get a string representation of this object.
		'''
		return "%f, %f, %f" % (self.x, self.y, self.z)

	def __getitem__ (self, name):
		return self.__dict__[name]

	def __add__ (self, other):
		if isinstance (other, Vector):
			return Vector (
				self.x + other.x,
				self.y + other.y,
				self.z + other.z
			)
		else:
			return Vector (
				self.x + other,
				self.y + other,
				self.z + other
			)

	def __sub__ (self, other):
		if isinstance (other, Vector):
			return Vector (
				self.x - other.x,
				self.y - other.y,
				self.z - other.z
			)
		else:
			return Vector (
				self.x - other,
				self.y - other,
				self.z - other
			)

	## Public Methods ##
	def cross (self, other):
		return Vector (
			self.y * other.z - self.z * other.y,
			- self.x * other.z + self.z * other.x,
			self.x * other.y - self.y * other.x
			)

	def dot (self, other):
		return self.x * other.x + self.y * other.y + self.z * other.z

	def magnitude (self):
		return sqrt (x ** 2 + y ** 2 + z ** 2)

	def unit (self):
		mag = self.magnitude ()
		return Vector (x / mag, y / mag, z / mag)


class Vertex (DataObject):
	## Overrides ##
	def __init__ (self, x = 0, y = 0, z = 0, **properties):
		DataObject.__init__ (self, **properties)
		self.position = Vector (x, y, z)

	def __getattr__ (self, name):
		if name in ("x", "y", "z"):
			return self.position[name]
		else:
			return DataObject.__getattr__ (self, name)

	def __setattr__ (self, name, value):
		if name in ("x", "y", "z"):
			self.position[name] = value
		else:
			DataObject.__setattr__ (self, name, value)

class Polygon (DataObject):
	## Overrides ##
	def __init__ (self, *args, **properties):
		DataObject.__init__ (self, **properties)
		self.texture = None

		for arg in args:
			if isinstance (arg, DataObject) and arg.getType () in ("Vertex", "VertexRef"):
				self.addChild (arg)
			else:
				raise "Trying to add an object that isn't a Vertex while creating Polygon."

class Mesh (DataObject):
	## Overrides ##
	def __init__ (self, *args, **properties):
		DataObject.__init__ (self, **properties)

		for arg in args:
			if isinstance (arg, DataObject) and arg.getType () in ("Vertex", "VertexRef", "Polygon", "PolygonRef"):
				self.addChild (arg)
			else:
				raise "Trying to add an object that isn't a Vertex or Polygon while creating Mesh."

class Texture (DataObject):
	## Overrides ##
	def __init__ (self, name, **properties):
		DataObject.__init__ (self, **properties)
		self.name = name

	def __str__ (self):
		''' Get a string representation of this object.
		'''
		return self.name

class Sector (DataObject):
	## Overrides ##
	def __init__ (self, *args, **properties):
		DataObject.__init__ (self, **properties)

		for arg in args:
			if isinstance (arg, DataObject) and arg.getType () in ("Vertex", "VertexRef", "Polygon", "PolygonRef", "Mesh", "MeshRef"):
				self.addChild (arg)
			else:
				raise "Trying to add an object that isn't a Vertex or Polygon while creating Sector."

class Level (DataObject):
	## Overrides ##
	def __init__ (self, *args, **properties):
		DataObject.__init__ (self, **properties)

		for arg in args:
			if isinstance (arg, DataObject) and arg.getType () in ("Vertex", "Polygon", "Sector", "Texture"):
				self.addChild (arg)
			else:
				raise "Trying to add an object that isn't a Vertex, Polygon, Sector, or Texture while creating Level."


import weakref
from types import *

class DataObject (object):
	''' Generic content object class.
	'''

	ignoredKeys = (
		"_DataObject__children",
		"_DataObject__parent",
		"_DataObject__listeners",
		"_DataObjectModel__index"
	)

	def __init__ (self, **properties):
		''' Initialize this object.
		'''
		self.__parent = None
		self.__children = list ()
		self.__listeners = list ()
		for key, value in properties.iteritems ():
			self.__dict__[key] = value

	def __str__ (self):
		''' Get a string representation of this object.
		'''
		outstr = "<%s" % self.getType ()
		for key, value in self.getAttributes ():
			outstr += " %s=\"%s\"" % (key, value)
		outstr += " />"
		return outstr

	def __setitem__ (self, item, value):
		dict.__setitem__ (self, item, value)
		for listener in self.__listeners:
			listener (item, value)

	def __setParent (self, parent):
		''' Set the parent of this object.
		'''
		if type (parent) is NoneType:
			self.__parent = None
		else:
			self.__parent = weakref.ref (parent)

	def getType (self):
		return self.__class__.__name__

	def getName (self):
		if "name" in self.__dict__:
			return self.name + " (" + self.getType () + ")"
		return self.getType ()

	def getParent (self):
		''' Get the parent of this object.
		'''
		if type (self.__parent) is NoneType:
			return None
		else:
			return self.__parent ()

	def addChild (self, child):
		''' Attach a child to this object.
		'''
		child.__setParent (self)
		self.__children.append (child)

	def getChild (self, index):
		''' Get the index'th child of this object.
		'''
		return self.__children[index]

	def getChildCount (self):
		''' Get the number of children of this object.
		'''
		return len (self.__children)

	def getChildCountRecursive (self):
		''' Get the number of children of this object and its children.
		'''
		count = self.getChildCount ()
		for child in self.__children:
			count += child.getChildCountRecursive ()
		return count

	def getChildren (self):
		''' Get a list of the children of this object.
		'''
		return self.__children

	def removeChild (self, child):
		''' Remove the given child of this object.
		'''
		if Object in child.__bases__:
			child.__setParent (None)
			self.__children.remove (child)
		else:
			self.__children[child].__SetParent (None)
			del self.__children[child]

	def getAttributes (self):
		''' Get a list of the attributes of this object.
		'''
		return [(key, val) for (key, val) in self.__dict__.iteritems () if key not in DataObject.ignoredKeys]

	def addListener (self, listener):
		''' Attach a listener to this object.
		'''
		self.__listeners.append (listener)

	def removeListener (self, listener):
		''' Remove a listener from this object.
		'''
		self.__listeners.remove (listener)


class DataObjectRef (DataObject):
	''' Generic content object reference class.
	'''

	ignoredKeys = (
		"_DataObject__children",
		"_DataObject__parent",
		"_DataObject__listeners",
		"_DataObjectRef__reference",
		"_DataObjectModel__index"
	)

	def __init__ (self, dataObject, **properties):
		''' Initialize this object.
		'''
		DataObject.__init__ (self, **properties)
		self.setTarget (dataObject)

	def __str__ (self):
		''' Get a string representation of this object.
		'''
		outstr = "<%s" % self.getType ()
		for key, value in self.getAttributes ():
			outstr += " %s=\"%s\"" % (key, value)
		outstr += " />"
		return outstr

	def __getattr__ (self, name):
		attribs = self.__reference.getAttributes ()
		if name in [x[0] for x in attribs]:
			return getattr (self.__reference, name)
		else:
			return getattr (super (DataObjectRef, self), name)

	def setTarget (self, dataObject):
		self.__reference = dataObject

	def getTarget (self):
		return self.__reference

	def getType (self):
		return self.__reference.__class__.__name__ + "Ref"

	def getName (self):
		if "name" in self.__dict__:
			return self.name + " (" + self.getType () + ") ->" + self.getTargetName ()
		return self.getType () + " -> " + self.getTargetName ()

	def getTargetName (self):
		if "name" in self.__reference.__dict__:
			return self.__reference.name
		else:
			return id (self.__reference)

	def getAttributes (self):
		''' Get a list of the attributes of this object.
		'''
		return [(key, val) for (key, val) in self.__dict__.iteritems () if key not in DataObjectRef.ignoredKeys] + [("target", self.__reference.name)]


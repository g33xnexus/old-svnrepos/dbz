#!/bin/sh
size=24

for file in `ls source`
do
	inkscape source/$file -w $size -h $size -e `echo $file | sed 's/\.[^.]*$//'`.png
done

__all__ = list ()

import re, os, os.path

qrcRegex = re.compile ("([^/]*)\.qrc$")

for qrcFile in os.listdir ("resources"):
	match = qrcRegex.search (qrcFile)
	if match:
		module = match.expand ("qrc_\\1")
		if not os.path.exists ("resources/" + module + ".py"):
			print "Compiling " + qrcFile
			os.system ("pyrcc4 resources/" + qrcFile + " > resources/" + module + ".py")
		__all__.append (module + ".py")
		__import__ ("resources." + module)


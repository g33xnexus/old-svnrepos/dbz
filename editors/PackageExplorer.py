from PyQt4 import QtCore, QtGui, uic
from libdbz import DataObjectModel
import resources
from widgets.GL3DView import *


class PackageExplorer (QtGui.QMainWindow, object):
	types = ("default", "group")
	(Icon, List, Details) = range (3)


	def __init__ (self, mainWindow, data = None, *args):
		apply (QtGui.QMainWindow.__init__, (self, ))

		self.mainWindow = mainWindow

		# Set up the user interface from Designer.
		self.ui = uic.loadUi ("layouts/PackageExplorer.ui", self)

		# Menu actions
		self.connect (self.ui.actionExit,
			QtCore.SIGNAL ("triggered()"),
			self.onQuit)
		self.connect (self.ui.treeView,
			QtCore.SIGNAL ("activated(QModelIndex)"),
			self.onActivate)

		self.model = DataObjectModel.DataObjectModel ()
		self.ui.treeView.setModel (self.model)

		## Open given data set ##
		self.edit (data)


	def edit (self, data):
		self.data = data
		self.model.setDataObject (data)


	def onActivate (self, modelIndex):
		''' An item has been activated, i.e. chosen by double clicking
		it with mouse or from keyboard.
		'''
		#TODO: Use this to bind double-click to opening a new editor,
		# if an appropriate one is registered:
		#if not self.mainWindow.Edit (self.model.getDataObject (modelIndex), true):
		#	# Then show it in the explorer, since we're already in the default editor.
		self.view = GL3DView (dataObject = self.model.getDataObject (modelIndex))
		self.view.show ()
		pass


	def onSelectionChanged (self, event):
		''' The selection has changed
		'''
		#TODO: Handle multiple selections!
		self.sidePane.Handle (self.ui.objectView.GetItemData (event.GetItem ()).GetData ())


	def onQuit (self):
		self.close ()

editor = PackageExplorer
